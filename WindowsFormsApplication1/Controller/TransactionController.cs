﻿using MiniProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MiniProject.Controller
{
    public class TransactionController
    {
        public bool withdraw(Account account, double amount)
        {
            using (DatabaseContext dataBaseContext = new DatabaseContext())
            {
                try
                {
                    account.withdraw(amount);
                    dataBaseContext.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }

        public bool withdraw(int accNo, double amount)
        {
            using (DatabaseContext dataBaseContext = new DatabaseContext())
            {
                try
                {
                    AccountController accCtr = new AccountController();
                    Account acc = accCtr.searchAccountByAccountNo(accNo);
                    dataBaseContext.Accounts.Attach(acc);
                    dataBaseContext.Entry(acc).State = System.Data.Entity.EntityState.Modified;

                    acc.withdraw(amount);
                    dataBaseContext.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }

        public bool deposit(Account account, double amount)
        {
            using (DatabaseContext dataBaseContext = new DatabaseContext())
            {
                try
                {
                    account.deposit(amount);
                    dataBaseContext.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }

        public bool deposit(int accNo, double amount)
        {
            using (DatabaseContext dataBaseContext = new DatabaseContext())
            {
                try
                {
                    AccountController accCtr = new AccountController();
                    Account acc = accCtr.searchAccountByAccountNo(accNo);
                    dataBaseContext.Accounts.Attach(acc);
                    dataBaseContext.Entry(acc).State = System.Data.Entity.EntityState.Modified;

                    acc.deposit(amount);
                    dataBaseContext.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }

        public bool addInterest(Account account)
        {
            using (DatabaseContext dataBaseContext = new DatabaseContext())
            {
                try
                {
                    account.addInterest();
                    dataBaseContext.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }
    }
}
