﻿using MiniProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MiniProject.Controller
{
    public class CustomerController
    {
        public bool addCustomer(Models.Customer customer)
        {
            using (DatabaseContext dataBaseContext = new DatabaseContext())
            {
                try
                {
                    dataBaseContext.Customers.Add(customer);
                    dataBaseContext.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }

        public Customer searchCustomerByName(string name)
        {
            using (DatabaseContext dataBaseContext = new DatabaseContext())
            {
                Customer customer = new Customer();
                customer = dataBaseContext.Customers.Where(p => p.Name == name).FirstOrDefault();
                if (customer != null)
                {
                    return customer;
                }
                else
                {
                    return null;
                }
            }
        }

        public Customer searchCustomerById(int id)
        {
            using (DatabaseContext dataBaseContext = new DatabaseContext())
            {
                Customer customer = new Customer();
                customer = dataBaseContext.Customers.Where(p => p.Id == id).SingleOrDefault();
                if (customer != null)
                {
                    return customer;
                }
                else
                {
                    return null;
                }
            }
        }

        //public IList<Customer> GetAllCustomers()
        //{
        //    using (var db = new DatabaseContext())
        //    {
        //        IList<Customer> returnObj = new List<Customer>();

        //        var query = from a in db.Customers select a;
        //        returnObj.Add(query);

        //        return 
        //    }
        //}

        public bool deleteCustomer(Customer customer)
        {
            using (DatabaseContext dataBaseContext = new DatabaseContext())
            {
                try
                {
                    var removeble = dataBaseContext.Customers.Find(customer.Id);
                    dataBaseContext.Customers.Remove(removeble);
                    dataBaseContext.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }

        public bool updateCustomer(Customer customer)
        {
            using(DatabaseContext dataBaseContext = new DatabaseContext()) {
                try
                {
                    //var toUpdate = dataBaseContext.Customers.Find(customer.Id);
                    dataBaseContext.Customers.Attach(customer);
                    dataBaseContext.Entry(customer).State = System.Data.Entity.EntityState.Modified;
                    dataBaseContext.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }
    }
}
