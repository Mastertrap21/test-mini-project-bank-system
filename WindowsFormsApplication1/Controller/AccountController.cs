﻿using MiniProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProject.Controller
{
    public class AccountController
    {
        public bool addAccount(Customer customer, string type)
        {
            using (DatabaseContext dataBaseContext = new DatabaseContext())
            {
                try
                {
                    Account account;
                    switch (type)
                    {
                        case "Youngster":
                            account = new Youngster();
                            account.InterestRate = 0.2;
                            account.Balance = 0;
                            account.Customer = customer;
                            dataBaseContext.Accounts.Add(account);
                            break;
                        case "EliteSaving":
                            account = new EliteSaving();
                            account.InterestRate = 0.5;
                            account.Balance = 0;
                            account.Customer = customer;
                            dataBaseContext.Accounts.Add(account);
                            break;
                        case "Senior":
                            account = new Senior();
                            account.InterestRate = 0.3;
                            account.Balance = 0;
                            account.Customer = customer;
                            dataBaseContext.Accounts.Add(account);
                            break;
                        case "ChildSaving":
                            account = new ChildSaving();
                            account.InterestRate = 0.1;
                            account.Balance = 0;
                            account.Customer = customer;
                            dataBaseContext.Accounts.Add(account);
                            break;
                    }
                    dataBaseContext.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }

        public bool addAccountGUI(int id, Account account)
        {
            using (DatabaseContext dataBaseContext = new DatabaseContext())
            {
                try
                {
                    CustomerController customerCtr = new CustomerController();
                    Customer customer = customerCtr.searchCustomerById(id);
                    dataBaseContext.Customers.Attach(customer);
                    dataBaseContext.Entry(customer).State = System.Data.Entity.EntityState.Modified;

                    account.Customer = customer;
                    dataBaseContext.Accounts.Add(account);
                    dataBaseContext.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }

        public Account searchAccountByAccountNo(int accountNo)
        {
            using (DatabaseContext dataBaseContext = new DatabaseContext())
            {
                Account account;
                account = dataBaseContext.Accounts.Where(p => p.AccountNo == accountNo).FirstOrDefault();
                if (account != null)
                {
                    return account;
                }
                else
                {
                    return null;
                }
            }
        }

        public bool deleteAccount(Account account)
        {
            using (DatabaseContext dataBaseContext = new DatabaseContext())
            {
                try
                {
                    var removeble = dataBaseContext.Accounts.Find(account.Id);
                    dataBaseContext.Accounts.Remove(removeble);
                    dataBaseContext.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }

        public bool updateAccount(Account account)
        {
            using (DatabaseContext dataBaseContext = new DatabaseContext())
            {
                try
                {
                    dataBaseContext.Accounts.Attach(account);
                    dataBaseContext.Entry(account).State = System.Data.Entity.EntityState.Modified;
                    dataBaseContext.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }

        public bool withdrawFromAccount(Account account, float sum)
        {
            using (DatabaseContext dataBaseContext = new DatabaseContext())
            {
                try
                {
                    dataBaseContext.Accounts.Attach(account);
                    account.withdraw(sum);
                    dataBaseContext.Entry(account).State = System.Data.Entity.EntityState.Modified;
                    dataBaseContext.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }

        public bool depositToAccount(Account account, float sum)
        {
            using (DatabaseContext dataBaseContext = new DatabaseContext())
            {
                try
                {
                    dataBaseContext.Accounts.Attach(account);
                    account.deposit(sum);
                    dataBaseContext.Entry(account).State = System.Data.Entity.EntityState.Modified;
                    dataBaseContext.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }
    }
}
