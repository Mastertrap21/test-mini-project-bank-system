﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProject.Models
{
    public partial class Senior
    {
        public override bool withdraw(double amount)
        {
            if (amount <= 0)
            {
                return false;
            }
            double newAmount = this.Balance - amount;
            if (newAmount < 0)
            {
                return false;
            }
            this.Balance = newAmount;
            return true;
        }
        public override bool deposit(double amount)
        {
            if (amount <= 0)
            {
                return false;
            }
            double newAmount = this.Balance + amount;
            this.Balance = newAmount;
            return true;
        }
        public override bool addInterest()
        {
            if (this.Balance > 10000)
            {
                double before10kBalance = this.Balance + this.Balance * (this.InterestRate + 0.5/100);
                double after10kBalance = this.Balance - 10000 + this.Balance * (this.InterestRate + 0.1/100);
                this.Balance = before10kBalance + after10kBalance;
            }
            else
            {
                if (this.Balance < 10000) {
                double newBalance = this.Balance + this.Balance * (this.InterestRate + 0.5/100);
                this.Balance = newBalance;
                }
            }
            return true;
        }
    }
}
