﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProject.Models
{
    public partial class Customer
    {
        public int Age
        {
            get
            {
                int calcAge = DateTime.Now.Year - this.DateOfBirth.Year;
                if (DateTime.Now.Month < this.DateOfBirth.Month || (DateTime.Now.Month == this.DateOfBirth.Month && DateTime.Now.Day < this.DateOfBirth.Day)) calcAge--;
                return calcAge;
            }
        }
        
    }
}
