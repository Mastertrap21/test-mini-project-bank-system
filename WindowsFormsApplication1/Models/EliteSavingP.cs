﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProject.Models
{
    public partial class EliteSaving
    {
        public override bool withdraw(double amount)
        {
            if (amount <= 0)
            {
                return false;
            }
            double newAmount = this.Balance - amount;
            if (newAmount < 500000)
            {
                return false;
            }
            this.Balance = newAmount;
            return true;
        }
        public override bool deposit(double amount)
        {
            if (amount <= 0)
            {
                return false;
            }
            double newAmount = this.Balance + amount;
            this.Balance = newAmount;
            return true;
        }
        public override bool addInterest()
        {
            if (this.Balance > 500000) {
                double before5kBalance = this.Balance + this.Balance * this.InterestRate;
                double after5kBalance = this.Balance - 5000000 + this.Balance * (this.InterestRate + 0.2/ 100);
                this.Balance = before5kBalance + after5kBalance;
            }
            return true;
        }
    }
}
