﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProject.Models
{
    public partial class Youngster
    {
        public double HigherInterestRate
        {
            get { return 0.5; }
        }
        

        public override bool withdraw(double amount)
        {
            if (amount <= 0)
            {
                return false;
            }
            double newAmount = this.Balance - amount;
            if (newAmount < 0)
            {
                return false;
            }
            this.Balance = newAmount;
            return true;
        }
        public override bool deposit(double amount)
        {
            if (amount <= 0)
            {
                return false;
            }
            double newAmount = this.Balance + amount;
            this.Balance = newAmount;
            return true;
        }
        public override bool addInterest()
        {
            double newAmount;
            if (this.Customer.Age < 25 && this.Balance < 250000)
            {
                 newAmount = this.Balance + this.Balance * HigherInterestRate;
            }
            else
            {
                 newAmount = this.Balance + this.Balance * this.InterestRate;
             }
            this.Balance = newAmount;
            return true;
        }
    }
}
