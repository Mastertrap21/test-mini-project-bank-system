﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProject.Models
{
    public partial class ChildSaving
    {
        public override bool withdraw(double amount)
        {
            if (this.Customer.Age < 18)
            {
                return false;
            }
            if (amount <= 0)
            {
                return false;
            }
            double newAmount = this.Balance - amount;
            if (newAmount < 0)
            {
                return false;
            }
            this.Balance = newAmount;
            return true;
        }
        public override bool deposit(double amount)
        {
            if (amount <= 0)
            {
                return false;
            }
            if (this.Customer.Age < 18 && amount > 5000)
            {
                return false;
            }
            double newAmount = this.Balance + amount;
            this.Balance = newAmount;
            return true;
        }
        public override bool addInterest()
        {
            double newAmount = this.Balance + this.Balance * this.InterestRate;
            this.Balance = newAmount;
            return true;
        }
    }
}
