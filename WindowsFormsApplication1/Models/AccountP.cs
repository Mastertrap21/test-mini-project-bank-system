﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProject.Models
{
    public abstract partial class Account
    {
        public abstract bool withdraw(double amount);
        public abstract bool deposit(double amount);
        public abstract bool addInterest();
    }
}
