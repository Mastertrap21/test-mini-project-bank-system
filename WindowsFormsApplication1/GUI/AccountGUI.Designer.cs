﻿namespace MiniProject.GUI
{
    partial class AccountGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.customer_lbl = new System.Windows.Forms.Label();
            this.customer_txt = new System.Windows.Forms.TextBox();
            this.create_btn = new System.Windows.Forms.Button();
            this.interest_txt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.balance_txt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.customer_list = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.type_combo = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // customer_lbl
            // 
            this.customer_lbl.AutoSize = true;
            this.customer_lbl.Location = new System.Drawing.Point(355, 15);
            this.customer_lbl.Name = "customer_lbl";
            this.customer_lbl.Size = new System.Drawing.Size(54, 13);
            this.customer_lbl.TabIndex = 0;
            this.customer_lbl.Text = "Customer:";
            // 
            // customer_txt
            // 
            this.customer_txt.Location = new System.Drawing.Point(415, 12);
            this.customer_txt.Name = "customer_txt";
            this.customer_txt.Size = new System.Drawing.Size(141, 20);
            this.customer_txt.TabIndex = 1;
            // 
            // create_btn
            // 
            this.create_btn.Location = new System.Drawing.Point(415, 124);
            this.create_btn.Name = "create_btn";
            this.create_btn.Size = new System.Drawing.Size(141, 23);
            this.create_btn.TabIndex = 2;
            this.create_btn.Text = "Create";
            this.create_btn.UseVisualStyleBackColor = true;
            this.create_btn.Click += new System.EventHandler(this.create_btn_Click);
            // 
            // interest_txt
            // 
            this.interest_txt.Location = new System.Drawing.Point(415, 38);
            this.interest_txt.Name = "interest_txt";
            this.interest_txt.Size = new System.Drawing.Size(141, 20);
            this.interest_txt.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(343, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Interest rate:";
            // 
            // balance_txt
            // 
            this.balance_txt.Location = new System.Drawing.Point(415, 64);
            this.balance_txt.Name = "balance_txt";
            this.balance_txt.Size = new System.Drawing.Size(141, 20);
            this.balance_txt.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(360, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Balance:";
            // 
            // customer_list
            // 
            this.customer_list.FormattingEnabled = true;
            this.customer_list.Location = new System.Drawing.Point(12, 12);
            this.customer_list.Name = "customer_list";
            this.customer_list.Size = new System.Drawing.Size(305, 433);
            this.customer_list.TabIndex = 7;
            this.customer_list.SelectedIndexChanged += new System.EventHandler(this.customer_list_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(336, 93);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Account type:";
            // 
            // type_combo
            // 
            this.type_combo.FormattingEnabled = true;
            this.type_combo.Items.AddRange(new object[] {
            "Youngster",
            "ChildSaving",
            "EliteSaving",
            "Senior"});
            this.type_combo.Location = new System.Drawing.Point(415, 90);
            this.type_combo.Name = "type_combo";
            this.type_combo.Size = new System.Drawing.Size(141, 21);
            this.type_combo.TabIndex = 9;
            // 
            // AccountGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 462);
            this.Controls.Add(this.type_combo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.customer_list);
            this.Controls.Add(this.balance_txt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.interest_txt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.create_btn);
            this.Controls.Add(this.customer_txt);
            this.Controls.Add(this.customer_lbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "AccountGUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AccountGUI";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AccountGUI_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label customer_lbl;
        private System.Windows.Forms.TextBox customer_txt;
        private System.Windows.Forms.Button create_btn;
        private System.Windows.Forms.TextBox interest_txt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox balance_txt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox customer_list;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox type_combo;
    }
}