﻿namespace MiniProject.GUI
{
    partial class MainGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.customer_add_btn = new System.Windows.Forms.Button();
            this.account_add_btn = new System.Windows.Forms.Button();
            this.deposit_btn = new System.Windows.Forms.Button();
            this.withdraw_btn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // customer_add_btn
            // 
            this.customer_add_btn.Location = new System.Drawing.Point(12, 12);
            this.customer_add_btn.Name = "customer_add_btn";
            this.customer_add_btn.Size = new System.Drawing.Size(97, 53);
            this.customer_add_btn.TabIndex = 0;
            this.customer_add_btn.Text = "Add Customer";
            this.customer_add_btn.UseVisualStyleBackColor = true;
            this.customer_add_btn.Click += new System.EventHandler(this.customer_add_btn_Click);
            // 
            // account_add_btn
            // 
            this.account_add_btn.Location = new System.Drawing.Point(115, 12);
            this.account_add_btn.Name = "account_add_btn";
            this.account_add_btn.Size = new System.Drawing.Size(97, 53);
            this.account_add_btn.TabIndex = 1;
            this.account_add_btn.Text = "Add Account";
            this.account_add_btn.UseVisualStyleBackColor = true;
            this.account_add_btn.Click += new System.EventHandler(this.account_add_btn_Click);
            // 
            // deposit_btn
            // 
            this.deposit_btn.Location = new System.Drawing.Point(12, 71);
            this.deposit_btn.Name = "deposit_btn";
            this.deposit_btn.Size = new System.Drawing.Size(97, 53);
            this.deposit_btn.TabIndex = 2;
            this.deposit_btn.Text = "Deposit";
            this.deposit_btn.UseVisualStyleBackColor = true;
            this.deposit_btn.Click += new System.EventHandler(this.deposit_btn_Click);
            // 
            // withdraw_btn
            // 
            this.withdraw_btn.Location = new System.Drawing.Point(116, 71);
            this.withdraw_btn.Name = "withdraw_btn";
            this.withdraw_btn.Size = new System.Drawing.Size(97, 53);
            this.withdraw_btn.TabIndex = 3;
            this.withdraw_btn.Text = "Withdraw";
            this.withdraw_btn.UseVisualStyleBackColor = true;
            this.withdraw_btn.Click += new System.EventHandler(this.withdraw_btn_Click);
            // 
            // MainGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(225, 256);
            this.Controls.Add(this.withdraw_btn);
            this.Controls.Add(this.deposit_btn);
            this.Controls.Add(this.account_add_btn);
            this.Controls.Add(this.customer_add_btn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainGUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainGUI_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button customer_add_btn;
        private System.Windows.Forms.Button account_add_btn;
        private System.Windows.Forms.Button deposit_btn;
        private System.Windows.Forms.Button withdraw_btn;
    }
}