﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniProject.GUI
{
    public partial class MainGUI : Form
    {
        public MainGUI()
        {
            InitializeComponent();
        }

        private void customer_add_btn_Click(object sender, EventArgs e)
        {
            CustomerGUI cust = new CustomerGUI();
            this.Hide();
            cust.ShowDialog();
        }

        private void account_add_btn_Click(object sender, EventArgs e)
        {
            AccountGUI acc = new AccountGUI();
            this.Hide();
            acc.ShowDialog();
        }

        private void MainGUI_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void deposit_btn_Click(object sender, EventArgs e)
        {
            DepositGUI acc = new DepositGUI();
            this.Hide();
            acc.ShowDialog();
        }

        private void withdraw_btn_Click(object sender, EventArgs e)
        {
            WithdrawGUI acc = new WithdrawGUI();
            this.Hide();
            acc.ShowDialog();
        }
    }
}
