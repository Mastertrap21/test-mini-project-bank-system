﻿using MiniProject.Controller;
using MiniProject.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniProject.GUI
{
    public partial class AccountGUI : Form
    {
        public AccountGUI()
        {
            InitializeComponent();

            using (var db = new DatabaseContext())
            {
                var query = from a in db.Customers select a;

                customer_list.Items.Clear();
                customer_list.Items.AddRange(query.ToArray());
                customer_list.DisplayMember = "Name";
                customer_list.ValueMember = "Id";
            }
        }

        private void AccountGUI_FormClosed(object sender, FormClosedEventArgs e)
        {
            MainGUI main = new MainGUI();
            main.Show();
            this.Close();
        }

        private void create_btn_Click(object sender, EventArgs e)
        {
            if (type_combo.SelectedIndex == 0)
            {
                AccountController ac = new AccountController();

                Youngster young = new Youngster();
                young.Balance = Convert.ToDouble(balance_txt.Text);
                young.InterestRate = Convert.ToDouble(interest_txt.Text);

                int tempId = Convert.ToInt32(customer_txt.Text);

                ac.addAccountGUI(tempId, young);
            }

            if (type_combo.SelectedIndex == 1)
            {
                AccountController ac = new AccountController();

                ChildSaving child = new ChildSaving();
                child.Balance = Convert.ToDouble(balance_txt.Text);
                child.InterestRate = Convert.ToDouble(interest_txt.Text);

                int tempId = Convert.ToInt32(customer_txt.Text);

                ac.addAccountGUI(tempId, child);
            }

            if (type_combo.SelectedIndex == 2)
            {
                AccountController ac = new AccountController();

                EliteSaving elite = new EliteSaving();
                elite.Balance = Convert.ToDouble(balance_txt.Text);
                elite.InterestRate = Convert.ToDouble(interest_txt.Text);

                int tempId = Convert.ToInt32(customer_txt.Text);

                ac.addAccountGUI(tempId, elite);
            }

            if (type_combo.SelectedIndex == 3)
            {
                AccountController ac = new AccountController();

                Senior senior = new Senior();
                senior.Balance = Convert.ToDouble(balance_txt.Text);
                senior.InterestRate = Convert.ToDouble(interest_txt.Text);

                int tempId = Convert.ToInt32(customer_txt.Text);

                ac.addAccountGUI(tempId, senior);
            }
        }

        private void customer_list_SelectedIndexChanged(object sender, EventArgs e)
        {
            Customer cust = (Customer)customer_list.SelectedItem;
            customer_txt.Text = cust.Id.ToString();
        }
    }
}
