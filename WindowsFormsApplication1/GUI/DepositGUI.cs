﻿using MiniProject.Controller;
using MiniProject.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniProject.GUI
{
    public partial class DepositGUI : Form
    {
        public DepositGUI()
        {
            InitializeComponent();

            using (var db = new DatabaseContext())
            {
                var query = from a in db.Accounts.OrderBy(x => x.AccountNo) select a;

                deposit_list.Items.Clear();
                deposit_list.Items.AddRange(query.ToArray());
                deposit_list.DisplayMember = "AccountNo";
                deposit_list.ValueMember = "AccountNo";
            }
        }

        private void DepositGUI_FormClosed(object sender, FormClosedEventArgs e)
        {
            MainGUI main = new MainGUI();
            main.Show();
            this.Close();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Account acc = (Account)deposit_list.SelectedItem;
            acc_txt.Text = acc.AccountNo.ToString();
            balance_txt.Text = acc.Balance.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TransactionController tc = new TransactionController();
            int tempAcc = Convert.ToInt32(acc_txt.Text);
            double tempAmount = Convert.ToDouble(ammount_txt.Text);

            tc.deposit(tempAcc, tempAmount);

            refreshList();
        }

        private void refreshList()
        {
            using (var db = new DatabaseContext())
            {
                var query = from a in db.Accounts.OrderBy(x => x.AccountNo) select a;

                deposit_list.Items.Clear();
                deposit_list.Items.AddRange(query.ToArray());
                deposit_list.DisplayMember = "AccountNo";
                deposit_list.ValueMember = "AccountNo";
            }
        }
    }
}
