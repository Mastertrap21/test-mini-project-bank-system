﻿namespace MiniProject
{
    partial class CustomerGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.name_txt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.street_txt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.zip_txt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.city_txt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.email_txt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.phone_txt = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.birthday_date = new System.Windows.Forms.DateTimePicker();
            this.create_btn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // name_txt
            // 
            this.name_txt.Location = new System.Drawing.Point(72, 12);
            this.name_txt.Name = "name_txt";
            this.name_txt.Size = new System.Drawing.Size(200, 20);
            this.name_txt.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Birthdate:";
            // 
            // street_txt
            // 
            this.street_txt.Location = new System.Drawing.Point(72, 64);
            this.street_txt.Name = "street_txt";
            this.street_txt.Size = new System.Drawing.Size(200, 20);
            this.street_txt.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Street:";
            // 
            // zip_txt
            // 
            this.zip_txt.Location = new System.Drawing.Point(72, 90);
            this.zip_txt.Name = "zip_txt";
            this.zip_txt.Size = new System.Drawing.Size(200, 20);
            this.zip_txt.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "ZipCode:";
            // 
            // city_txt
            // 
            this.city_txt.Location = new System.Drawing.Point(72, 116);
            this.city_txt.Name = "city_txt";
            this.city_txt.Size = new System.Drawing.Size(200, 20);
            this.city_txt.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(39, 119);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "City:";
            // 
            // email_txt
            // 
            this.email_txt.Location = new System.Drawing.Point(72, 142);
            this.email_txt.Name = "email_txt";
            this.email_txt.Size = new System.Drawing.Size(200, 20);
            this.email_txt.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 145);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Email:";
            // 
            // phone_txt
            // 
            this.phone_txt.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.phone_txt.Location = new System.Drawing.Point(72, 168);
            this.phone_txt.Name = "phone_txt";
            this.phone_txt.Size = new System.Drawing.Size(200, 20);
            this.phone_txt.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 171);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "PhoneNo:";
            // 
            // birthday_date
            // 
            this.birthday_date.Location = new System.Drawing.Point(72, 38);
            this.birthday_date.Name = "birthday_date";
            this.birthday_date.Size = new System.Drawing.Size(200, 20);
            this.birthday_date.TabIndex = 14;
            // 
            // create_btn
            // 
            this.create_btn.Location = new System.Drawing.Point(103, 194);
            this.create_btn.Name = "create_btn";
            this.create_btn.Size = new System.Drawing.Size(90, 23);
            this.create_btn.TabIndex = 15;
            this.create_btn.Text = "Add Customer";
            this.create_btn.UseVisualStyleBackColor = true;
            this.create_btn.Click += new System.EventHandler(this.create_btn_Click);
            // 
            // CustomerGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 234);
            this.Controls.Add(this.create_btn);
            this.Controls.Add(this.birthday_date);
            this.Controls.Add(this.phone_txt);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.email_txt);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.city_txt);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.zip_txt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.street_txt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.name_txt);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "CustomerGUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Customer";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CustomerGUI_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox name_txt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox street_txt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox zip_txt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox city_txt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox email_txt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox phone_txt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker birthday_date;
        private System.Windows.Forms.Button create_btn;
    }
}

