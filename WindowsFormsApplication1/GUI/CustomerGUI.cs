﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiniProject.Controller;
using MiniProject.Models;
using MiniProject.GUI;

namespace MiniProject
{
    public partial class CustomerGUI : Form
    {
        public CustomerGUI()
        {
            InitializeComponent();
        }

        private void create_btn_Click(object sender, EventArgs e)
        {
            try
            {
                CustomerController cc = new CustomerController();
                Customer cm = new Customer();

                cm.City = city_txt.Text;
                cm.DateOfBirth = birthday_date.Value;
                cm.Email = email_txt.Text;
                cm.Name = name_txt.Text;
                cm.PhoneNo = phone_txt.Text;
                cm.Street = street_txt.Text;
                cm.ZipCode = Convert.ToInt32(zip_txt.Text);

                cc.addCustomer(cm);
                this.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CustomerGUI_FormClosed(object sender, FormClosedEventArgs e)
        {
            MainGUI main = new MainGUI();
            main.Show();
            this.Close();
        }
    }
}
