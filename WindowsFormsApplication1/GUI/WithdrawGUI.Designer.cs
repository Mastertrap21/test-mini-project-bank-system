﻿namespace MiniProject.GUI
{
    partial class WithdrawGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.withdraw_list = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.acc_txt = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.ammount_txt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.balance_txt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // withdraw_list
            // 
            this.withdraw_list.FormattingEnabled = true;
            this.withdraw_list.Location = new System.Drawing.Point(12, 13);
            this.withdraw_list.Name = "withdraw_list";
            this.withdraw_list.Size = new System.Drawing.Size(287, 433);
            this.withdraw_list.TabIndex = 0;
            this.withdraw_list.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(326, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Account No. :";
            // 
            // acc_txt
            // 
            this.acc_txt.Location = new System.Drawing.Point(405, 39);
            this.acc_txt.Name = "acc_txt";
            this.acc_txt.Size = new System.Drawing.Size(136, 20);
            this.acc_txt.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(405, 91);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(136, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Withdraw";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ammount_txt
            // 
            this.ammount_txt.Location = new System.Drawing.Point(405, 65);
            this.ammount_txt.Name = "ammount_txt";
            this.ammount_txt.Size = new System.Drawing.Size(136, 20);
            this.ammount_txt.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(353, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Amount:";
            // 
            // balance_txt
            // 
            this.balance_txt.Enabled = false;
            this.balance_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.balance_txt.Location = new System.Drawing.Point(405, 13);
            this.balance_txt.Name = "balance_txt";
            this.balance_txt.Size = new System.Drawing.Size(136, 20);
            this.balance_txt.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(350, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Balance:";
            // 
            // WithdrawGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(553, 458);
            this.Controls.Add(this.balance_txt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ammount_txt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.acc_txt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.withdraw_list);
            this.Name = "WithdrawGUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WithdrawGUI";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DepositGUI_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox withdraw_list;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox acc_txt;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox ammount_txt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox balance_txt;
        private System.Windows.Forms.Label label3;
    }
}