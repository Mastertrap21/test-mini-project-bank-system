﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MiniProject.Controller;
using MiniProject.Models;

namespace UnitTestProject1
{
    [TestClass]
    public class TestCustomer
    {
        [TestMethod]
        public void TestAddCustomer()
        {
            CustomerController customerController = new CustomerController();
            Customer customer = new Customer();
            customer.Name = "Razvan";
            customer.DateOfBirth = new DateTime(2008, 6, 1, 7, 47, 0);
            customer.Street = "Brandevej 126";
            customer.ZipCode = 9220;
            customer.City = "Aalborg";
            customer.Email = "crs.razvan@yahoo.com";
            customer.PhoneNo = "9220898";
            bool response = customerController.addCustomer(customer);
            Assert.AreEqual(true, response);
            Customer response2 = customerController.searchCustomerByName("Razvan");
            Assert.IsNotNull(response2);
        }

        [TestMethod]
        public void TestSearchCustomerByName()
        {
            CustomerController customerController = new CustomerController();
            Customer response = customerController.searchCustomerByName("Razvan");
            Assert.IsNotNull(response);
        }

         [TestMethod]
        public void TestDeleteCustomer()
        {
            CustomerController customerController = new CustomerController();
            Customer customer = customerController.searchCustomerByName("Razvan");
            bool response = customerController.deleteCustomer(customer);
            Assert.AreEqual(true, response);
        }

         [TestMethod]
         public void TestUpdateCustomer()
         {
             CustomerController customerController = new CustomerController();
             Customer customer = customerController.searchCustomerByName("Razvan");
             customer.Email = "test@gmail.com";
             customerController.updateCustomer(customer);
             Customer customer2 = customerController.searchCustomerByName("Razvan");
             Assert.AreEqual(customer.Email,customer2.Email);
         }
    }
}
