﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MiniProject.Controller;
using MiniProject.Models;

namespace UnitTestProject1
{
    [TestClass]
    public class TestAccount
    {
        [TestMethod]
        public void TestAddAccount()
        {
            AccountController accountController = new AccountController();
            CustomerController customerController = new CustomerController();
            Customer customer = customerController.searchCustomerByName("Razvan");
            bool response = accountController.addAccount(customer, "Youngster");
            Assert.AreEqual(true, response);
            Account response2 = accountController.searchAccountByAccountNo(40000001);
            Assert.IsNotNull(response2);
            response = accountController.addAccount(customer, "EliteSaving");
            Assert.AreEqual(true, response);
            Account Response2 = accountController.searchAccountByAccountNo(20000001);
            Assert.IsNotNull(response2);
            response = accountController.addAccount(customer, "ChildSaving");
            Assert.AreEqual(true, response);
            response2 = accountController.searchAccountByAccountNo(40000001);
            Assert.IsNotNull(response2);
            response = accountController.addAccount(customer, "Senior");
            Assert.AreEqual(true, response);
            response2 = accountController.searchAccountByAccountNo(30000001);
            Assert.IsNotNull(response2); 
        }

        [TestMethod]
        public void TestSearchAccountByAccountNumber()
        {
            AccountController accountController = new AccountController();
            Account response = accountController.searchAccountByAccountNo(0);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void TestDeleteAccount()
        {
            AccountController accountController = new AccountController();
            Account account = accountController.searchAccountByAccountNo(0);
            bool response = accountController.deleteAccount(account);
            Assert.AreEqual(true, response);
        }

        [TestMethod]
        public void TestUpdateAccount()
        {
            AccountController accountController = new AccountController();
            Account account = accountController.searchAccountByAccountNo(0);
            account.Balance = 2;
            accountController.updateAccount(account);
            Account account2 = accountController.searchAccountByAccountNo(0);
            Assert.AreEqual(account.Balance, account2.Balance);
        }

        [TestMethod]
        public void TestWithdrawFromAccount()
        {
            AccountController accountController = new AccountController();
            Account account = accountController.searchAccountByAccountNo(40000001);
            bool response = accountController.withdrawFromAccount(account,200);
            Assert.IsTrue(response);
        }

        [TestMethod]
        public void TestDepositToAccount()
        {
            AccountController accountController = new AccountController();
            Account account = accountController.searchAccountByAccountNo(40000001);
            bool response = accountController.depositToAccount(account, 1000);
            Assert.IsTrue(response);
        }


    }
}
